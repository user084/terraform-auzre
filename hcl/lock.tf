resource "azurerm_management_lock" "resource-group-level" {
  name       = "resource-group-level"
  scope      = azurerm_resource_group.rg.id
  lock_level = "ReadOnly"
  notes      = "This Resource Group is Read-Only"
}
