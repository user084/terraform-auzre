variable "VM_NAME" {
  default = "TestVM"
}

variable "VM_ADMIN" {
  default = "azure-admin"
}

variable "LOCATION" {
  default = "switzerlandnorth"
}
